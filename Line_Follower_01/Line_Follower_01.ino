#define lights_on_LED 8
int LDR_r, LDR_c, LDR_l;

int left_offset=0, right_offset=0, centre=0;
int speed1 = 3, speed2 =11;
int direction1 = 12, direction2 = 13;

int initial_speed = 70;
int rotation_offset = 30;

int sensor_threshold = 5;

int left_motor_speed = initial_speed;
int right_motor_speed = initial_speed;

void setup(){
  pinMode(lights_on_LED, OUTPUT);
  pinMode(speed1, OUTPUT);
  pinMode(speed2, OUTPUT);
  pinMode(direction1, OUTPUT);
  pinMode(direction2, OUTPUT);
  
  sensor_calibration();
  
  delay(3000);
  digitalWrite(lights_on_LED, HIGH);
  
  digitalWrite(direction1, HIGH);
  digitalWrite(direction2, HIGH);
  
  analogWrite(speed1, left_motor_speed);
  analogWrite(speed2, right_motor_speed);  
}

void loop(){
  left_motor_speed = initial_speed;
  right_motor_speed = initial_speed;
  
  LDR_l = analogRead(0)+left_offset;
  LDR_c = analogRead(1);
  LDR_r = analogRead(2)+right_offset;
  
  if(LDR_l>(LDR_c + sensor_threshold)){
    left_motor_speed = initial_speed + rotation_offset;
    right_motor_speed = initial_speed - rotation_offset;
  }
  
  if(LDR_r>(LDR_c + sensor_threshold)){
    left_motor_speed = initial_speed - rotation_offset;
    right_motor_speed = initial_speed + rotation_offset;
  }
  
  analogWrite(speed1, left_motor_speed);
  analogWrite(speed2, right_motor_speed);

}

void sensor_calibration(){
  for(int x = 1;x<15;x++){
    digitalWrite(lights_on_LED, HIGH);
    delay(100);
    LDR_l = analogRead(0);
    LDR_c = analogRead(1);
    LDR_r = analogRead(2);
    
    left_offset = left_offset + LDR_l;
    centre = centre + LDR_c;
    right_offset = right_offset + LDR_r;
    
    delay(100);
    digitalWrite(lights_on_LED, LOW);
    delay(100);
    
  }
    left_offset = left_offset / 15;
    right_offset = right_offset / 15;
    
    left_offset = centre - left_offset;
    right_offset = centre - right_offset;

}  
