

#define input_L1 4
#define input_L2 2
#define input_R1 7
#define input_R2 6
#define speed_input 5

int adjust_speed;

void setup(){
  pinMode(input_L1, OUTPUT);
  pinMode(input_L2, OUTPUT);
  pinMode(input_R1, OUTPUT);
  pinMode(input_R2, OUTPUT);
  pinMode(speed_input, OUTPUT); 
}

void loop(){
  adjust_speed = analogRead(3)/4;
  delay(10);
  
  analogWrite(speed_input, adjust_speed);
  byte incoming_data;
  incoming_data = Serial.read();
  // 1 HIGH and 2 LOW -> CLOCKWISE
  // 1 LOW and 2 HIGH -> ANTI-CLOCKWIse
  if(incoming_data == 'W'){
    digitalWrite(input_L1, HIGH);
    digitalWrite(input_L2, LOW);
    
    digitalWrite(input_R1, HIGH);
    digitalWrite(input_R2, LOW);
  }
  
  else if (incoming_data == 'A'){
    digitalWrite(input_L1, LOW);
    digitalWrite(input_L2, LOW);
    
    digitalWrite(input_R1, HIGH);
    digitalWrite(input_R2, LOW);
  }
  
  else if (incoming_data == 'S'){
    digitalWrite(input_L1, LOW);
    digitalWrite(input_L2, HIGH);
    
    digitalWrite(input_R1, LOW);
    digitalWrite(input_R2, HIGH);
  }
  
  else if (incoming_data == 'D'){
    digitalWrite(input_L1, HIGH);
    digitalWrite(input_L2, LOW);
    
    digitalWrite(input_R1, LOW);
    digitalWrite(input_R2, LOW);
  }
  
  else{
    // KEEP THIS FOR CONNECTION TO BLUE PIN
  }
}
