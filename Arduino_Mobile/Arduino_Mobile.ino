#include <Servo.h>
#include <Math.h>
//This code is for Arduino_Mobile Device.
//Abbreviation: URF - Ultrasonic Range Finder
//              PWM - Pulse Width Modulation
//              Rx - Receiver

//Declaring pins
#define dc_motor_inp1 2
#define dc_motor_inp2 3
#define dc_motor_inp3 4
#define dc_motor_inp4 5
#define  servo_m 6
#define urf_front 7
#define urf_right 8
#define distance_adjust 9
#define auto_mode 10
#define mobile_mode 11
#define buzzer_sound 12

//Declaring Right URF
long URF_Right;
int upper_threshold = 10;
int lower_threshold = 8;
int critical_right = 9;

//Declaring Front URF
long URF_Front;
int critical_front = 10;

//constant value distance before what car has to be on designed/desired path
#define distance_before_line 3

//Declaring angle
int phi_angle;

//Creating servo Object
Servo servo_main;

//Declaring mode_select bit either 1 or 0. 1=Auto mode; and 0=Remote control mode
boolean mode_select;

//Declaring variable for remote control mode
char received_character;

//Declaring variables for counting occurences of received character from base station.
int count_W = 0;
int count_A = 0;
int count_S = 0;

//Declaring angles
int angle1 = 15;
int angle2 = 30;
int angle3 = 45;
int angle4 = 60;

//functions declarations
void auto_mode();
void remote_control_mode();
int table_lookup(int);
void move_forward();
void move_reverse();
int table_lookup(int);

//mode_select is connected to pin 4.
//URF_Right is connected to analog pin 1.
//URF_Front is connected to analog pin 2.
//phi_angle is connected to PWM pin 5.

//For rear two DC motors. pin configurations for motor driver IC
//Input 1 pin in motor driver IC is connected to pin 6 in Arduino
//Input 2 pin in motor driver IC is connected to pin 7 in Arduino
//Input 3 pin in motor driver IC is connected to pin 8 in Arduino
//Input 4 pin in motor driver IC is connected to pin 9 in Arduino

void setup()
{
  
  //Begin Serial communication through XBee
  //Baud rate set to 9600
  Serial.begin(9600);
  //connect servo motor output to PWM pin 5
  servo_main.attach(5);
  
  //define mode for pin 4 which is mode_select pin.
  pinMode(4,INPUT);
  
  //pin definitions for pins 6,7,8,9.
  pinMode(6,OUTPUT);
  pinMode(7,OUTPUT);
  pinMode(8,OUTPUT);
  pinMode(9,OUTPUT);
  
  //initial direction for DC motor
  //move forward
  digitalWrite(6,HIGH);
  digitalWrite(7,LOW);
  digitalWrite(8,HIGH);
  digitalWrite(9,LOW);
  
}

void loop()
{
  
  //read pin 4 to determine mode : auto mode OR remote control mode.
  mode_select = digitalRead(4);
  
  switch(mode_select)
  {
    case HIGH: 
    //read distances from URF_Front and URF_Right
            URF_Right = (long)(analogRead(1)/2.54);
            URF_Front = (long)(analogRead(2)/2.54);
    //If mode_select reads 1 or HIGH, go to auto_mode
            auto_mode();
            delay(50);
            break;
    case LOW:
    //read character received from Rx pin
            received_character = Serial.read();
    //If mode_select reads 0 or LOW, go to remote_control_mode
            remote_control_mode();
            delay(50);
            break;
  }
}

void auto_mode()
{
  int URF_Front_temp,URF_Right_temp;
  URF_Front_temp =critical_front + distance_before_line;
  //A1 -- Refer documentation for further details
  //If URF_Front is atleast (critical_limit+distance_before_line) far from wall
  if(URF_Front > URF_Front_temp)
  {
    //B11
    //If URF_Right is less than lower_threshold or between lower_threshold and critical_right
    if(URF_Right < lower_threshold || URF_Right < critical_right)
    {
      //refer documentation for calculating phi_angle.
      URF_Right_temp = critical_right-URF_Right;
      URF_Right_temp = URF_Right_temp/distance_before_line;
      phi_angle = asin(URF_Right_temp);
      //turn servo by phi_angle calculated above.
      servo_main.write(phi_angle);
    }
    
    //B12
    //If URF_Right is 1 inch out of upper threshold, give a sharp turn.
    else if(URF_Right > upper_threshold + 1)
    {
      phi_angle = 30;
      servo_main.write(phi_angle);
    }
    //B13
    //If URF_Right is less than upper threshold and greater than critical limit.
    else if(URF_Right < upper_threshold && URF_Right > critical_right)
    {
      URF_Right_temp = URF_Right-critical_right;
      URF_Right_temp = URF_Right_temp/distance_before_line;
      phi_angle = asin(URF_Right_temp);
      servo_main.write(phi_angle);
    }
    
    //B14
    //If URF_Right is critical distance far from wall (as desired) then do not turn. just move forward.
    else 
    {
      phi_angle = 0;
      servo_main.write(phi_angle);
    }
    
  } //End of A1
  
  //A2
  //If URF_Front is less than critical_front limit
  else if(URF_Front < critical_front)
  {
    //B21
    //If URF_Right is less than critical right value
    if(URF_Right < critical_right || URF_Right > critical_right)
    {
      phi_angle = atan(URF_Front/URF_Right);
      servo_main.write(phi_angle);
    }
    
    //B22
    //If URF_Right equals critical
    else
    {
      phi_angle = atan(10/9);
      servo_main.write(phi_angle);
    }
    
  }//End of A2
  
  //If URF_Front equals critical front, turn 90 degree.
  else
  {
    phi_angle = 90;
    servo_main.write(phi_angle);
  }
  
}//End of Auto mode function

//start of remote control mode
void remote_control_mode()
{
  switch(received_character)
  { 
    //if received character is 'W'
    case 'W': 
        move_forward();
        count_W++;
        //table lookup is a function that takes integer value as parameter and returns integer.
        //Returned integer contains value that would make turn servo at that angle.
        phi_angle = table_lookup(count_W);
        //based on value returned, turn front wheel by "turn" degrees
        servo_main.write(phi_angle);
        //set other variable count_x to ZERO
        count_A = 0;
        count_S = 0;
        break;
       
    //If received character is 'A'   
    case 'A': 
        move_forward();
        count_A++;
        phi_angle = table_lookup(count_A);
        servo_main.write(phi_angle);
        //set other variable count_x to ZERO
        count_W = 0;
        count_S = 0;
        break;
        
    //If received character is 'S' 
    case 'S':
        move_forward();
        count_S++;
        phi_angle = table_lookup(count_S);
        servo_main.write(phi_angle);
        //set other variable count_x to ZERO
        count_W = 0;
        count_A = 0;
        break;
     
    //If received character is 'D'
    case 'D':
        //direction for DC motor on receiving 'D'
        //move reverse
        move_reverse();
        //INCASE IF ANY PIN REMAINS EMPTY ATTACH RED LIGHT TO SHOW REVERSE TAKEN.
        break;
        
  }
}//End of remote_control_mode() function.

//this function is self explanatory. It moves forward
void move_forward()
{
  //move forward
  digitalWrite(6,HIGH);
  digitalWrite(7,LOW);
  digitalWrite(8,HIGH);
  digitalWrite(9,LOW);
}

//this function is self explanatory. It moves forward
void move_reverse()
{
  //move reverse
  digitalWrite(6,LOW);
  digitalWrite(7,HIGH);
  digitalWrite(8,LOW);
  digitalWrite(9,HIGH);
}

int table_lookup(int count)
{
  if(count >= 1 && count < 20)
    return angle1;
  
  else if(count >= 20 && count < 40)
     return angle2;
   
  else if(count >= 40 && count < 70)
     return angle3;
     
  else 
     return angle4;    
}
