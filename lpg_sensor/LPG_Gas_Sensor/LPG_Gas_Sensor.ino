/*
LPG Gas Sensor

Parts Required:
  1. One LPG Gas Sensor: MQ-6
  2. 10K ohm resistor
  3. 5V battery
  
Coded by: Krutarth Patel
*/


void setup(){
  pinMode(3,INPUT);
}
void loop(){
  byte analog_pin_lpg;
  analog_pin_lpg = byte(analogRead(3) * 100);
  
  Serial.print('0x7A');
  Serial.print(analog_pin_lpg);
}
