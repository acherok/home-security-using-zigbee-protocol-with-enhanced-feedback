/*
Arduino_Coordinator
** Make Web page
** Extract Data
** Update Data on Web page
** Blink Green LED while(in actual before) receiving data

Data will be updated every 5 seconds on web page
Requirements: 
  * Arduino Uno
  * Arduino Ethernet Sheild
  * Green Led - To indicate that data has received
  * Red Led - To indicate LOW battery at End Device Radio
  
Sketch Size: 11728 bytes
Coded By: Krutarth Patel
*/
//SPI and Ethernet library for server communication
#include <SPI.h>
#include <Ethernet.h>

byte green_led_pin = 2;
//for indicating status of battery at End device radio
byte battery_status = 3;
//media access control and ip of computer connected to
byte mac[] = {0x5C,0xAC,0x4C,0x27,0x9E,0x32};
byte ip[] = {10,100,90,35};

/*
to find port number in windows 7 operating System,follow the instruction-
1)Choose Start>Control Panel. Click the System and Security link and then click Windows Firewall.
2)On the left, click the Advanced Settings link.
3)On the left, click Inbound Rules. Then, on the right, under Actions, click the New Rule link.
4)select the option marked Port and click Next.
5)In the Specific Local Ports box, type the ports you want to open, separated by commas, and then click Next.
6)Choose Allow the Connection and click Next.
7)Check the boxes for Private or any other desired network type, and then click Next.
8)Type a name (usually the name of whatever program required the opening). Click Finish.
*/

//start server on PORT 159 (Other port numbers: 145,147)
EthernetServer server(159);

void setup(){
  //Begin Ethernet and Server
  Ethernet.begin(mac,ip);
  server.begin();
  //Pin definitions
  pinMode(green_led_pin, OUTPUT);     //green_led_pin pin set to OUTPUT
  pinMode(battery_status, OUTPUT);    //battery_status pin set to OUTPUT
}

void loop(){
  //variables for LPG gas sensor
  byte lpg_sensor_data;
  //variables for smoke sensor 
  byte smoke_sensor_data_H,smoke_sensor_data_L;
  int smoke_sensor_data;
  //variables for sound sensor 
  byte sound_sensor_data_H,sound_sensor_data_L;
  int sound_sensor_data;
  //variables for LDR sensor
  byte ldr_data_H,ldr_data_L;
  int ldr_data;
  //variable assigned for storing not-necessary values received from End Device Radio
  byte discard_data;
  
  /*-----------------EXTRACTING DATA WHICH IS RECEIVED FROM END DEVICE RADIO-----------------------*/
  //default assumption that data coming will consume two bytes. WILL CHANGE LATER ON
  // Based on above assumption total bytes received: 20
  if(Serial.available()>19){
    byte incomingData = Serial.read();

    if(incomingData == 0x7A){// meaning data from lpg gas sensor
      blink_green_led();
      lpg_sensor_data = Serial.read();
    } // END of LPG gas sensor data 
         
    if(incomingData == 0x7B){//meaning data from smoke sensor
      blink_green_led();
      for(int i=0;i<16;i++){
         discard_data = Serial.read();
      }
      smoke_sensor_data_H = Serial.read();
      smoke_sensor_data_L = Serial.read();
      smoke_sensor_data = int(smoke_sensor_data_H + smoke_sensor_data_L);
    } // END of Smoke gas sensor data
  
    if(incomingData == 0x7C){//meaning data from sound sensor
      blink_green_led();
      for(int i=0;i<16;i++){
         discard_data = Serial.read();
      }
      sound_sensor_data_H = Serial.read();
      sound_sensor_data_L = Serial.read();
      sound_sensor_data = int(sound_sensor_data_H + sound_sensor_data_L);
    } // END of Sound sensor data
 
     if(incomingData == 0x7D){//meaning data from LDR sensor
      blink_green_led();
      for(int i=0;i<16;i++){
         discard_data = Serial.read();
      }
      ldr_data_H = Serial.read();
      ldr_data_L = Serial.read();
      ldr_data = int(ldr_data_H + ldr_data_L);
    } // END of ldr sensor data
     
     if(incomingData == 0x7E){//meaning data containing Battery heath value
      digitalWrite(battery_status, HIGH); //turn red led ON, indicating replacement of battery or recharging
    }
  }
  /*-----------END of Extracting data------------*/
  
  /*-----------SERVER MAINTENANCE----------------*/
  EthernetClient client1 = server.available();
  if(client1){
    //since an http request ends with a blank line
    boolean blankLine = true;
    while(client1.connected()){
      if(client1.available()){
        char c = client1.read();
        if(c=='\n' && blankLine){
          client1.println("HTTP/1.1 200 OK");
          client1.println("Content-Type: text/html \n");
          client1.println("<html><head><META HTTP-EQUIV=""refresh""Content=""5"">\n");
          client1.println("<title>Housafe server</title></head>");
          client1.println("<body>\n");
          client1.println("<h1>Housafe server");
          client1.println("<h3>Parameters Value");
          client1.println("lpg_gas_sensor::> ");
          client1.println(lpg_sensor_data);
          if(lpg_sensor_data <=36){
            client1.println("Seems to be Gas leakage around. Please check !!");
          }
          else if(lpg_sensor_data >=50 && lpg_sensor_data<60){
            client1.println("There is slight lpg gas detected !! ");
          }
          else{
            client1.println("No Emergency...!!!");
          }
          client1.println("\n/*-------------------------------------------*/\n");
          client1.println("smoke_gas_sensor::> ");
          client1.println(smoke_sensor_data);
          client1.println("\n/*-------------------------------------------*/\n");
          client1.println("sound_sensor::> ");
          client1.println(sound_sensor_data);
          client1.println("\n/*-------------------------------------------*/\n");
          client1.println("ldr_sensor::> ");
          client1.println(ldr_data);
          client1.println("\n/*-------------------------------------------*/\n");
          break;
        }
        if(c=='\n'){blankLine = true;}
        else if(c != '\r'){blankLine = false;}
      }
    }
    delay(10); 
    client1.stop();
  }
  
}

void blink_green_led(){
  digitalWrite(green_led_pin,HIGH);
  delay(10);
  digitalWrite(green_led_pin,LOW);
  delay(10);
  digitalWrite(green_led_pin,HIGH);
  delay(10);
  digitalWrite(green_led_pin,LOW);
  delay(10);
}
