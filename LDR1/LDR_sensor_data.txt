LDR sensor

Light Dependent Resistor (LDR) also called as CdS (Cadmium-sulphide) or a photoresistor.
Each of the legs on the LDR goes  to an electrode. Between a dark er material, making a squiggly line between the electrodes, is the ph otoconductive material. The compon ent has a transparent plastic or glass coating. When light hits the photoconductive mater ial, it loses its resistance, allowing more current to flow between the electrodes.

Parts
	20K ohm LDR sensor

Concept - Voltage divider

Image: B:\Housafe\Project\LDR\Volatage_Divider
The voltage in (Vin) is connected across both resistors. When you measure  the voltage across one of the resistors (Vout) it will be less (divided). The formula for working out what the voltage at Vout comes out when measured across R2 is:
	Vout = R2*Vin/(R2 + R1)

Circuit
Refer Image: B:\Housafe\Project\LDR\LDR_circuit

Arduino Code
	byte ldr_data, ldr_data_pin =5;
	void setup(){
		pinMode(ldr_data_pin, INPUT);
		Serial.begin(9600);
	}
	void loop(){
		ldr_data = analogRead(ldr_data_pin) * 10;
		Serial.print('0x7D');
		Serial.print(ldr_data);
	}