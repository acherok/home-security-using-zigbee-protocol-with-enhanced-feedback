/*
LDR Sensor

Parts:
10K ohm resistor
20K ohm LDR

Coded by: Krutarth Patel
*/

byte ldr_data, ldr_data_pin =5;
void setup(){
  pinMode(ldr_data_pin, INPUT);
  Serial.begin(9600);
}
void loop(){
  ldr_data = analogRead(ldr_data_pin) * 10;
  Serial.print('0x7D');
  Serial.print(ldr_data);
}
